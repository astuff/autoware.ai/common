/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Authors: Simon Thompson, Ryohsuke Mitsudome
 */

#include <lanelet2_extension/projection/enu_projector.h>
#include <ros/ros.h>

#include <vector>
#include <set>
#include <string>
#include <utility>

namespace lanelet
{
namespace projection
{
EnuProjector::EnuProjector(Origin origin) : Projector(origin)
{
  earth_ = GeographicLib::Geocentric::WGS84();

  // Setup ECEF to ENU transform based on provided origin
  ecef_enu_tf_ = getEcefToEnuTransform(
    origin.position.lat, origin.position.lon, origin.position.ele).inverse();
}

EnuProjector::EnuProjector(tf2::Transform origin_frame_tf) : Projector(Origin({ 0.0, 0.0 }))
{
  earth_ = GeographicLib::Geocentric::WGS84();

  // Setup ECEF to ENU transform based on provided transform
  ecef_enu_tf_ = origin_frame_tf;
}

BasicPoint3d EnuProjector::forward(const GPSPoint& gps) const
{
  // Convert LLH to ECEF
  double x, y, z;
  earth_.Forward(gps.lat, gps.lon, gps.ele, x, y, z);

  // Transform from ECEF to local ENU frame
  tf2::Vector3 pos_ecef(x, y, z);
  tf2::Vector3 pos_enu = ecef_enu_tf_ * pos_ecef;
  BasicPoint3d enu_point(pos_enu.getX(), pos_enu.getY(), pos_enu.getZ());

  return enu_point;
}

GPSPoint EnuProjector::reverse(const BasicPoint3d& enu_point) const
{
  // Transform ENU to ECEF
  tf2::Vector3 pos_enu(enu_point.x(), enu_point.y(), enu_point.z());
  tf2::Vector3 pos_ecef = ecef_enu_tf_.inverse() * pos_enu;

  // Convert ECEF to LLH
  double lat, lon, hgt;
  earth_.Reverse(pos_ecef.getX(), pos_ecef.getY(), pos_ecef.getZ(), lat, lon, hgt);
  GPSPoint gps{ lat, lon, hgt };

  return gps;
}

tf2::Transform EnuProjector::getEcefToEnuTransform(double latitude, double longitude, double height)
{
  // This function is inspired by:
  // https://github.com/wavelab/libwave/tree/master/wave_geography

  std::vector<double> rotation_vec(9, 0.0);
  double x, y, z;
  earth_.Forward(latitude, longitude, height, x, y, z, rotation_vec);

  tf2::Vector3 origin(x, y, z);
  tf2::Matrix3x3 rotation;

  rotation.setValue(
      rotation_vec[0],
      rotation_vec[1],
      rotation_vec[2],
      rotation_vec[3],
      rotation_vec[4],
      rotation_vec[5],
      rotation_vec[6],
      rotation_vec[7],
      rotation_vec[8]);

  tf2::Transform ecef_enu_tf(rotation, origin);
  return ecef_enu_tf;
}

}  // namespace projection
}  // namespace lanelet
